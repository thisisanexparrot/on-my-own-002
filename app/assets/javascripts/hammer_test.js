$(document).ready(function(){

  document.getElementById("changeThis").innerHTML = "Javascript time bros!";
  var myElement = document.getElementById('myElement');
  myElement.innerHTML = "My element";
  var x = 0;
  // create a simple instance
  // by default, it only adds horizontal recognizers
  var mc = new Hammer(myElement);

  // let the pan gesture support all directions.
  // this will block the vertical scrolling on a touch-device while on the element
  mc.get('pan').set({ direction: Hammer.DIRECTION_ALL });

  // listen to events...
  mc.on("panleft panright panup pandown tap press", function(ev) {
      myElement.textContent = ev.type +" gesture detected DUDES.";
      x = x+1;
      if(ev.type == "panright")
      {
        x=0;
        myElement.height = 200;
      }
      document.getElementById("changeThis").innerHTML = x;
  });


  var myClassTests = document.getElementsByClassName("classesTest");
  for (var i = 0; i < myClassTests.length; i++) {
    myClassTests[i].innerHTML = "Yooo";
  }

});
