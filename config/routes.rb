Rails.application.routes.draw do
  get 'sessions/new'
  get 'users/new'

  get '/helpForNoobs',    to: 'static_pages#help'
  get '/about',     to: 'static_pages#about'
  get '/cats',      to: 'static_pages#cats'
  get '/contact',   to: 'static_pages#contact'
  get '/signUp',    to: 'users#new'

  get '/login',     to: 'sessions#new'
  post '/login',    to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  root 'static_pages#home'# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :users
end
